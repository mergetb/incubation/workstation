package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	"google.golang.org/protobuf/encoding/protojson"

	"gitlab.com/mergetb/xir/v0.3/go"
)

func runGoModel(src string) ([]byte, error) {

	dir, err := ioutil.TempDir("/tmp", "gocode")
	if err != nil {
		return nil, err
	}

	err = ioutil.WriteFile(filepath.Join(dir, "go.mod"), []byte(gomod), 0644)
	if err != nil {
		return nil, err
	}

	err = ioutil.WriteFile(filepath.Join(dir, "main.go"), []byte(runner), 0644)
	if err != nil {
		return nil, err
	}

	err = os.MkdirAll(filepath.Join(dir, "usercode"), 0755)
	if err != nil {
		return nil, err
	}

	err = ioutil.WriteFile(filepath.Join(dir, "/usercode/topo.go"), []byte(src), 0644)
	if err != nil {
		return nil, err
	}

	cmd := exec.Command("go", "run", "main.go")
	cmd.Dir = dir
	out, err := cmd.Output()
	if err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			return nil, CompilationError{Message: string(exiterr.Stderr)}
		}
	}

	fa, err := xir.FacilityFromB64String(string(out))
	if err != nil {
		fmt.Println(string(out))
		return nil, fmt.Errorf("facility from b64: %v", err)
	}

	js, err := protojson.Marshal(fa)
	if err != nil {
		return nil, err
	}

	return js, nil

}

var gomod = `module go.workstation.mergetb.dev/runner

go 1.15

require (
	gitlab.com/mergetb/xir v0.2.20-0.20210123035643-6edbe803c7d1
	google.golang.org/protobuf v1.25.0
)
replace gitlab.com/mergetb/xir => /space/ryv1/xir
replace gitlab.com/mergetb/portal/services => /space/ryv1/services
`

var runner = `package main

import (
	"fmt"
	"log"

	usercode "go.workstation.mergetb.dev/runner/usercode"
)

func main() {

	fa := usercode.Topo()

	buf, err := fa.ToB64Buf()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Print(string(buf))
}
`
