package main

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os/exec"
	"time"

	"google.golang.org/protobuf/encoding/protojson"

	"gitlab.com/mergetb/xir/v0.3/go"
)

func runPyModel(src string) ([]byte, error) {

	// Do not let the command take "too long" and steal resources.
	// Slight protection against DoS attack. 15 seconds is probably not enough
	// to protect against a determined attacker. C'est la vie.
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	findTopologyPy := "py2xir.py"
	cmd := exec.CommandContext(ctx, "python3", findTopologyPy)
	stdin, err := cmd.StdinPipe()
	if err != nil {
		log.Fatalf("faied to get python stdin pipe: %v", err)
	}

	go func() {
		defer stdin.Close()
		io.WriteString(stdin, src)
	}()

	out, err := cmd.Output()
	if err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			return nil, CompilationError{Message: string(exiterr.Stderr)}
		}
		return nil, fmt.Errorf("cmd error: %v", err)
	}

	ntwk, err := xir.NetworkFromB64String(string(out))
	if err != nil {
		return nil, err
	}

	js, err := protojson.Marshal(ntwk)
	if err != nil {
		return nil, err
	}

	return js, nil
}

func runPyModelFile(filename string) ([]byte, error) {

	out, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	return runPyModel(string(out))

}
