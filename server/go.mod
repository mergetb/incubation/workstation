module gitlab.com/mergetb/incubation/workstation

go 1.15

require (
	github.com/labstack/echo/v4 v4.1.17
	gitlab.com/mergetb/xir v0.2.20-0.20210201222631-b2fae4779ecb
	google.golang.org/protobuf v1.25.0
)
