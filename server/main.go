package main

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	//"gitlab.com/mergetb/xir/v0.3/go"
	//"google.golang.org/protobuf/encoding/protojson"
)

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())
	e.POST("/buildpy", buildpy)
	e.POST("/buildgo", buildgo)
	e.Logger.Fatal(e.Start(":8086"))
}

func buildpy(c echo.Context) error { return build(c, runPyModel) }
func buildgo(c echo.Context) error { return build(c, runGoModel) }

func build(c echo.Context, builder func(string) ([]byte, error)) error {

	src, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return fmt.Errorf("failed to read request: %v", err)
	}
	out, err := builder(string(src))
	if err != nil {
		if _, ok := err.(CompilationError); ok {
			return c.String(http.StatusBadRequest, err.Error())
		}
		return err
	}

	/*
		ntwk, err := xir.NetworkFromB64String(string(out))
		if err != nil {
			return err
		}

		js, err := protojson.Marshal(ntwk)
		if err != nil {
			return err
		}
	*/

	return c.JSONBlob(http.StatusOK, out)

}

type CompilationError struct {
	Message string
}

func (c CompilationError) Error() string {
	return c.Message
}
