import sys
from sys import stdin, stderr, exit
import os
import mergexp
import base64

# grab the well-known module-level global that has the topology.
from mergexp import __xp   
    
# execute the given python model script.
try:
    exec(stdin.read())
except Exception as e:
    print("{}".format(e), file=stderr)
    exit(10)

# make sure the __xp instance is OK.
if not isinstance(__xp['topo'], mergexp.Network):
    print("bad mergexp network %s"%(type(__xp['topo'])), file=stderr)
    print("netwk %s"%(__xp['topo']), file=stderr)
    exit(20)

topo = __xp['topo']

# base64 encode it so it's predictably printable and readable
print(base64.b64encode(topo.xir().SerializeToString()).decode())
exit(0)
