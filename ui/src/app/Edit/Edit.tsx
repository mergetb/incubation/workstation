import * as React from 'react';
import * as monaco from 'monaco-editor';
import { PageSection, Title } from '@patternfly/react-core';
import { CodeEditor, Language, CodeEditorControl } from '@patternfly/react-code-editor';
import { Alert, AlertActionCloseButton, AlertActionLink } from '@patternfly/react-core';
import PlayIcon from '@patternfly/react-icons/dist/js/icons/build-icon';
import { initVimMode } from 'monaco-vim';
import styles from './Edit.module.css';

function setFt(src, setFileType) {
  if (src.startsWith('from')) { setFileType('python'); }
  else if (src.startsWith('package')) { setFileType('go'); }
  else { setFileType('text'); }
}

function editorChange(src, setFileType) {

  window.localStorage.setItem('wks-code', src)
  setFt(src, setFileType)

}

function editorMounted(editor, monaco) {

  // Does not work :(
  //initVimMode(editor, document.getElementById('status'))

}

function handleBuildError(message, setBuildResult) {
  console.log(message);
  setBuildResult(
    <Alert variant="danger" title= "Build Error">
      <pre>{message}</pre>
    </Alert>
  );
}

function handleBuildSuccess(json, setBuildResult) {
  window.localStorage.setItem('wks-xir', JSON.stringify(json));
  setBuildResult(
    <Alert variant="success" title= "Build Succeeded"> </Alert>
  );
}

function build(src, setBuildResult, filetype) {

  if (filetype == 'python') {
    window.localStorage.setItem('wks-xir-type', 'network');
    buildsrc('buildpy', src, setBuildResult)
  }
  else if (filetype == 'go') {
    window.localStorage.setItem('wks-xir-type', 'facility');
    buildsrc('buildgo', src, setBuildResult)
  }

}

function buildsrc(path, src, setBuildResult) {

  console.log("building");

  fetch("http://localhost:8086/"+path, {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    headers: {
      'Content-Type': 'text/plain',
    },
    body: src,
  }).then(response => {
      if (!response.ok) {
        response.text().then(text => handleBuildError(text, setBuildResult));
      } else {
        response.json().then(json => handleBuildSuccess(json, setBuildResult));
      }
  })

}

function buildgo(src, setBuildResult) {

  handleBuildError('Go support not implemented yet!', setBuildResult);

}

const Edit: React.FunctionComponent = () => {

  const [buildResult, setBuildResult] = React.useState(<div></div>);

  const [src, setSrc] = React.useState(
    window.localStorage.getItem('wks-code')
  );

  const [filetype, setFileType] = React.useState('text')

  React.useEffect(() => {
    setFt(src, setFileType);
  }, [])


  const customControl = (
    <CodeEditorControl
      icon={<PlayIcon/>}
      aria-label="Build"
      toolTipText="Build"
      onClick={(source) => build(source, setBuildResult, filetype)}
      isVisible={src !== ''}
    />
  );

  return (
    <PageSection>
      <CodeEditor
        className={styles.editor}
        isUploadEnabled
        isDownloadEnabled
        isCopyEnabled
        isLanguageLabelVisible
        language={filetype}
        height='1000px'
        customControls={customControl}
        onChange={(src) => editorChange(src, setFileType)}
        onEditorDidMount={editorMounted}
        code={src}
      />
      {buildResult}
      <div id="status"></div>

    </PageSection>
  )

}

export { Edit };

