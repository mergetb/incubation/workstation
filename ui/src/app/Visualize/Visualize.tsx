import * as React from 'react';
import { PageSection, Title } from '@patternfly/react-core';
import { MultiGraph } from "graphology";
import { WebGLRenderer } from 'sigma';
import 'setimmediate';
import FA2Layout from "graphology-layout-forceatlas2/worker";
import randomLayout from "graphology-layout/random";
import styles from './Visualize.module.css';

function runLayout(layout: FA2Layout) {
    layout.start()
    setTimeout(() => layout.stop(), 1000)
}

function runLayoutWithFixed(layout: FA2Layout, graph, node) {
    layout.start()
    setTimeout(() => {
        layout.stop()
        if (node != null) {
            graph.setNodeAttribute(node, "fixed", 0);
        }
    }, 1000)
}

function renderNetwork(xir, dragging, draggedNode) {

  const graph = new MultiGraph();

  for (const x of xir.nodes) {
    graph.addNode(x.id, {label: x.id, size: 5});
  }

  let mpl_count = 0;

  for (const x of xir.links) {
    if(x.endpoints.length == 2) {
      graph.addEdge(
        x.endpoints[0].socket.element,
        x.endpoints[1].socket.element,
      );
    }
    if(x.endpoints.length > 2) {
      let mpl = "mpl"+mpl_count++;
      graph.addNode(mpl, {size: 10});
      for (const e of x.endpoints) {
        graph.addEdge(e.socket.element, mpl);
      }
    }
  }
  
  doRender(graph, dragging, draggedNode)

}

function doRender(graph, dragging, draggedNode) {

  randomLayout.assign(graph);

  const container = document.getElementById("container");
  const renderer = new WebGLRenderer(graph, container);

  const layout = new FA2Layout(graph, { 
      settings: { 
          barnesHutOptimize: true,
          slowDown: 20,
      } 
  });
  runLayout(layout);

  const camera = renderer.getCamera();

  const captor = renderer.getMouseCaptor();

  renderer.on("downNode", (e) => {
      dragging.current = true;
      draggedNode.current = e.node;
      if (draggedNode.current != null) {
          graph.setNodeAttribute(draggedNode.current, "fixed", 1);
      }
      camera.disable();
      //layout.start();
  });

  captor.on("mouseup", () => {
      runLayoutWithFixed(layout, graph, draggedNode.current);
      dragging.current = false;
      draggedNode.current = null;
      camera.enable();
      //layout.stop();
  });

  captor.on("mousemove", (e) => {
      if (!dragging.current) return;

      // Get new position of node
      const pos = renderer.normalizationFunction.inverse(
          camera.viewportToGraph(renderer, e.x, e.y)
      );

      graph.setNodeAttribute(draggedNode.current, "x", pos.x);
      graph.setNodeAttribute(draggedNode.current, "y", pos.y);
  });

}

function renderFacility(xir, dragging, draggedNode) {

  const graph = new MultiGraph();
  if (xir.resources !== undefined) {
    for (const x of xir.resources) {
      let color = "#222";
      if (x.roles.includes("InfraSwitch")) {
        if (x.roles.includes("Spine")) {
          color = "#0000ff";
        }
        if (x.roles.includes("Stem")) {
          color = "#6D6DFF";
        }
        if (x.roles.includes("Leaf")) {
          color = "#B6B6FF";
        }
      }
      if (x.roles.includes("XpSwitch")) {
        if (x.roles.includes("Spine")) {
          color = "#008000";
        }
        if (x.roles.includes("Stem")) {
          color = "#599859";
        }
        if (x.roles.includes("Leaf")) {
          color = "#8EB78E";
        }
      }
      if (x.roles.includes("Gateway")) {
        color = "#CB0606";
      }
      if (x.roles.includes("NetworkEmulator")) {
        color = "#267E81";
      }
      if (x.roles.includes("InfraServer")) {
        color = "#822626"
      }
      if (x.roles.includes("StorageServer")) {
        color = "#7949A1"
      }

      graph.addNode(x.id, {label: x.id, size: 5, color: color});
    }
  }

  if (xir.cables !== undefined ) {
    for (const x of xir.cables) {
      //TODO breakout cables
      graph.addEdge(
        x.ends[0].connectors[0].Port.element,
        x.ends[1].connectors[0].Port.element,
      )
    }
  }

  doRender(graph, dragging, draggedNode)

}

const Visualize: React.FunctionComponent = () => {

  let draggedNode = React.useRef();
  let dragging = React.useRef(false);

  React.useEffect(() => {

    let xir = window.localStorage.getItem('wks-xir');
    let xirtype = window.localStorage.getItem('wks-xir-type');
    console.log('xir');
    console.log(xir);
    console.log('xirtype: ' + xirtype);
    if (xir !== null) {
      xir = JSON.parse(xir);
      console.log(xir);
      if (xirtype == 'network') {
        renderNetwork(xir, dragging, draggedNode);
      } 
      else if (xirtype == 'facility') {
        renderFacility(xir, dragging, draggedNode);
      }
    }


  }, [])

  return (
      <PageSection>
        <div className={styles.vizdiv} id="container"> </div>
      </PageSection>
  )
}

export { Visualize };
