# MergeTB Workstation

An application for working with experiments topologies.

## Getting Started

You'll need the following things installed
- Python3 and python3-pip
- The Go programming langauge tools
- NPM

### Install XIR v0.3

```
git clone git@gitlab.com:mergetb/xir
cd xir
git checkout v1-ry
cd v0.3/mx
pip install --user .
```

### Build and run the workstation server

```
cd server
go build
./workstation
```

### Build and run the workstation web application

```
cd ui
npm install
npm run start:dev
```

This should open up a web browser to localhost:9000, if not open a web browser
to that address.

## Screenshot

![](doc/screenshot.png)
